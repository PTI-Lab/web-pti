# RIBBO DOCKERS #

## WHAT'S INCLUDED ##
### **Docker containers included:** ###
* ***Web:*** Flask python code and static resource files
* ***Nginx:*** Web server to deploy
* ***Data:*** Postgres instance
* ***Postgres:*** Container with database. **This helps ensure that the data persists even if the Postgres container is completely destroyed.**
* ***Wowza*** *(possible update)*

# HOW TO START ALL CONTAINERS #
To start and deploy all containers we have to execute the following command:

**```python clean-start.py```** *

\* For production mode sudo will be required

After the execution web containers will be exposed to:

* **```http://localhost:8080/```** in **development mode**
* **```http://domain-name/```** in **production mode** (default web port, 80) 

## REQUIREMENTS ##
* ***Python*** to execute script
* ***PIP*** to install library dependency (```sudo apt-get -y install python-pip```)

##  WHAT CLEAN-START DO ##
**```clean-start.python```** is the script responsible to orchestration of deployment. Its execution makes several things:

* Remove all existing containers to ensure the recreation.
* Build all container images based on docker-compose.yml or docker-compose-prod.yml depending of the environment we are deploying *(development or production)*.
* Start all containers based on the images created previously.
* Execute **```create_db.py```** file, responsible for creating all tables and creating first data.

### HOW TO SWITCH BETWEEN DEPLOYING MODES ###
To switch between deploying modes we have to modify ```/.env``` file, modifying ```ENV``` value between *dev* and *prod*.

## HOW TO TEST GCM DOWNSTREAM (TEMPORALLY) ##
```http://localhost:8080/gcm/downstream/token/{token}/message/{message}```

Replace *{token}* by the registration_id provided by android app registration and *{message}* by the message that you want to send.

Example:
http://localhost:8080/gcm/downstream/token/eJpELnlbNsU:APA91bFnZktsRbeHNQwPlcN3ofr8CzxTgzXlREKSygKLjIO5IDDpFAQ6iQ5G239sgpqTJ8Mxw2BRuXtKqpYVCuLUms8_AIqQF7DbocrJUk2cp_D8qXjVI5CjfpJwR1VvKXWvzSosgP9J/message/left

## HOW TO CONNECT TO DATABASE ##

The following command allow us to connect with our database:

```$ psql -h 192.168.99.100 -p 5432 -U postgres --password``` 

**IMPORTANT:** IP address corresponds to data container and may be different from 192.168.99.100


Folder structure:
```
#!file structure
├── docker-compose.yml
├── docker-compose-prod.yml
├── .env                  -- Environment variables  
├── nginx
│   ├── Dockerfile
│   └── sites-enabled
│       └── flask_project
└── web
    ├── Dockerfile
    ├── app.py
    ├── config.py
    ├── create_db.py
    ├── models.py          -- Database models
    ├── requirements.txt
    ├── static
    │   ├── css
    │   │   ├── bootstrap.min.css
    │   │   └── styles.css
    │   ├── img
    │   └── js
    │       ├── bootstrap.min.js
    │       └── main.js
    └── templates
        ├── _base.html   -- Footer y header
        └── *.html       -- Partial Renders with page contents

```

Project based on: [Dockerizing Flask](https://realpython.com/blog/python/dockerizing-flask-with-compose-and-machine-from-localhost-to-the-cloud/)