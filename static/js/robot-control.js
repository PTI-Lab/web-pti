var data = [];
var jw_width = "100%";
var rotation = 0;

// Outputs some logs about jwplayer
function print(t, obj) {
    for (var a in obj) {
        if (typeof obj[a] === "object")
            print(t + '.' + a, obj[a]);
        else
            data[t + '.' + a] = obj[a];
    }
}

$(document).ready(function () {
    var robot_name = $('#robot_name').val();
    jwplayer('player').setup({
        wmode: 'transparent',
        width: jw_width,
        aspectratio: "16:9",
        stretching: 'exactfit'
    });

    $('#btn_start').click(function () {
        startPlayer($('#stream_url').val());
    });

    $('#btn_stop').click(function () {
        jwplayer('player').stop();
    });

    $('.arrow.arrow-up').click(function () {
        send_gcm(robot_name, "UP");
    });
    $('.arrow.arrow-down').click(function () {
        send_gcm(robot_name, "DOWN");
    });
    $('.arrow.arrow-left').click(function () {
        send_gcm(robot_name, "LEFT");
    });
    $('.arrow.arrow-right').click(function () {
        send_gcm(robot_name, "RIGHT");
    });
    $('.circle').click(function () {
        send_gcm(robot_name, "STOP");
    });

    $('[class^=rotate-streaming]').click(function () {
        if (this.className.indexOf("right") != -1) {
            rotate_player(90);
        }
        else rotate_player(-90);
    });
    $(document).keydown(function (event) {
        switch (event.which) {
            case 38:
            case "W".charCodeAt(0):
                send_gcm(robot_name, "UP");
                event.preventDefault();
                return false;
                break;
            case 40:
            case "S".charCodeAt(0):
                send_gcm(robot_name, "DOWN");
                event.preventDefault();
                return false;
                break;
            case 37:
            case "A".charCodeAt(0):
                send_gcm(robot_name, "LEFT");
                event.preventDefault();
                return false;
                break;
            case 39:
            case "D".charCodeAt(0):
                send_gcm(robot_name, "RIGHT");
                event.preventDefault();
                return false;
                break;
            case 32:
            case 0:
                send_gcm(robot_name, "STOP");
                event.preventDefault();
                return false;
                break;
        }

    });
    startPlayer($('#stream_url').val());
});

// Starts the flash player
function startPlayer(stream) {

    jwplayer('player').setup({
        width: jw_width,
        aspectratio: "16:9",
        stretching: 'exactfit',
        sources: [{
            file: stream
        }],
        rtmp: {
            bufferlength: 3
        }
    });

    jwplayer("player").onMeta(function (event) {
        var info = "";
        for (var key in data) {
            info += key + " = " + data[key] + "<BR>";
        }
        print("event", event);
    });

    jwplayer('player').play();
}

function send_gcm(robot_name, msg) {
    var url = window.location.protocol + "//" + window.location.host + "/gcm/downstream/";
    $.post(
        url,
        {robot: robot_name, message: msg},
        "json"
    ).done(function (response) {
        if (response.error) {
            console.log('gcm message error message: ' + response.message);
            var url = window.location.protocol + "//" + window.location.host + "/logout/";
            if (response.message == "Your user is currently banned, please contact admins.") {
                window.location.replace(url);
            }
        }
        else {
            console.log('gcm message sent successfully');
        }
    }).error(function (response) {
        console.log('gcm error ' + response.message);
    });
}

function rotate_player(deg) {
    rotation += deg;
    $('#player object').css({
        '-webkit-transform': 'rotate(' + rotation + 'deg)',
        '-moz-transform': 'rotate(' + rotation + 'deg)',
        '-ms-transform': 'rotate(' + rotation + 'deg)',
        '-o-transform': 'rotate(' + rotation + 'deg)',
        'transform': 'rotate(' + rotation + 'deg)'
    });
}