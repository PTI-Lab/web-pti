# models.py


import datetime

from flask.ext.login import UserMixin
from werkzeug.security import generate_password_hash, \
    check_password_hash

from app import db


class User(db.Model, UserMixin):
    __tablename__ = 'users'

    id = db.Column(db.Integer, index=True, unique=True, primary_key=True)
    username = db.Column(db.String, nullable=False, unique=True)
    password = db.Column(db.String, nullable=False)
    active = db.Column(db.Boolean, nullable=False, default=True)
    authenticated = db.Column(db.Boolean, nullable=False, default=False)
    robots = db.relationship('Robot', backref='users', lazy='dynamic')
    admin = db.Column(db.Boolean, nullable=False, default=False)
    date_registred = db.Column(db.DateTime, nullable=False)
    date_lastlogin = db.Column(db.DateTime, nullable=True)

    def __init__(self, username, password, admin=False):
        self.username = username
        self.set_password(password)
        self.admin = admin
        self.date_registred = datetime.datetime.utcnow()

    def __repr__(self):
        return '<User %r>' % self.username

    def is_active(self):
        return self.active

    def get_id(self):
        return self.id

    def is_authenticated(self):
        return self.authenticated

    @staticmethod
    def is_anonymous():
        return False

    def logout(self):
        self.date_lastlogin = datetime.datetime.utcnow()
        self.authenticated = False

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def is_admin(self):
        return self.admin

    def release_robots(self):
        for robot in self.robots.all():
            robot.release_control()

    def change_state(self):
        self.active = not self.active


class Robot(db.Model):
    colors = {"Online": "success", "Offline": "danger", "In use": "warning"}

    __tablename__ = 'robots'

    id = db.Column(db.Integer, index=True, unique=True, primary_key=True)
    name = db.Column(db.String, nullable=False, unique=True)
    location = db.Column(db.String, nullable=False)
    active = db.Column(db.Boolean, nullable=False, default=True)
    in_use = db.Column(db.Boolean, nullable=False, default=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=True, default=None)
    token = db.Column(db.String, nullable=True, default=None)
    date_registred = db.Column(db.DateTime, nullable=False)
    date_last_use = db.Column(db.DateTime, nullable=True)

    def __init__(self, name, location, active):
        self.name = name.lower()
        self.location = location.lower()
        self.active = active
        self.date_registred = datetime.datetime.utcnow()

    def is_in_use(self):
        return self.in_use

    def is_active(self):
        return self.active

    def control(self, user_id):
        self.in_use = True
        self.user_id = user_id

    def release_control(self):
        self.in_use = False
        self.user_id = None
        self.date_last_use = datetime.datetime.utcnow()

    def can_release_control(self, user_id):
        return self.user_id == user_id or \
               (user_id and User.query.get(user_id).is_admin())

    def get_state(self):
        if self.is_active():
            if self.is_in_use():
                return 'In use'
            else:
                return 'Online'
        else:
            return 'Offline'

    def get_state_class(self):
        return self.colors[self.get_state()]
