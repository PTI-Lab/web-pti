# create_db.py


from app import app
from models import *

if app.debug:
	db.reflect()
	db.drop_all()


db.create_all()
db.session.commit()

#Default Users
users = [
	User('admin', 'admin', True),
	User('dani', 'dani')
]
for user in users:
	db.session.add(user)

#Default Robot
robots = [
	Robot('Ribbo', 'Spain', True),
	Robot('Rob', 'France', True),
	Robot('Spark', 'Nederland', False)
]
for robot in robots:
	db.session.add(robot)

db.session.commit()