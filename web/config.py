# config.py


import os


class BaseConfig(object):
	APP_NAME = os.environ['APP_NAME']
	SECRET_KEY = os.environ['SECRET_KEY']
	ENV = os.environ['ENV']
	if ENV == 'dev':
		DEBUG = True
		HOST = '0.0.0.0'
	else:
		DEBUG = False
	DB_NAME = os.environ['DB_NAME']
	DB_USER = os.environ['DB_USER']
	DB_PASS = os.environ['DB_PASS']
	DB_SERVICE = os.environ['DB_SERVICE']
	DB_PORT = os.environ['DB_PORT']
	SQLALCHEMY_DATABASE_URI = 'postgresql://{0}:{1}@{2}:{3}/{4}'.format(
		DB_USER, DB_PASS, DB_SERVICE, DB_PORT, DB_NAME
	)
	ANDROID_API_KEY = "AIzaSyA3mOb8wf8pY40L67eN0q_rqIgjEL3OQ88"

# class BaseConfig(object):
#     SECRET_KEY = 'hi'
#     DEBUG = True
#     DB_NAME = 'postgres'
#     DB_SERVICE = 'localhost'
#     DB_PORT = 5432
#     SQLALCHEMY_DATABASE_URI = 'postgresql://{0}:{1}/{2}'.format(
#         DB_SERVICE, DB_PORT, DB_NAME
#     )
