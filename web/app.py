# app.py


from flask import Flask, flash, redirect, render_template, \
	request, url_for, g, session, jsonify
from flask.ext.login import LoginManager, login_required, login_user, \
	logout_user, current_user
from flask.ext.sqlalchemy import SQLAlchemy
from gcm import *

from config import BaseConfig

app = Flask(__name__)
app.config.from_object(BaseConfig)
db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"
login_manager.login_message_category = "info"

from models import *


def get_api_key():
	return BaseConfig.ANDROID_API_KEY


@app.before_request
def before_request():
	g.user = current_user


@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))


@login_manager.unauthorized_handler
def unauthorized_callback():
	flash('Please log in to acces this page', 'info')
	session['next_url'] = request.path
	return redirect(login_manager.login_view)


@app.route('/', methods=['GET', 'POST'])
def index():
	return render_template('index.html')


@app.route('/login/', methods=['GET', 'POST'])
def login():
	if current_user.is_authenticated:
		flash('You are currently logged in', 'danger')
		return redirect(url_for('index'))

	if request.method == 'POST':
		username = request.form['username']
		password = request.form['password']
		user = User.query.filter_by(username=username).first()
		if user is not None:
			if user.check_password(password):
				try:
					if not user.is_active():
						flash('Your user is banned, please contact admins for more info.', 'danger')
						return redirect(url_for('index'))
					user.authenticated = True
					db.session.commit()
					remember_me = 'remember' in request.form
					login_user(user, remember_me)
					flash('You were successfully logged in', 'success')
				except:
					db.session.rollback()
					flash('Something gone wrong. Please try again', 'warning')
				target_url = False
				if 'next_url' in session:
					target_url = session.pop('next_url')
				return redirect(target_url or url_for('index'))
		flash('Invalid credentials', 'danger')
		return redirect(url_for('login'))
	return render_template('login.html')


@app.route('/signup/', methods=['GET', 'POST'])
def signup():
	if current_user.is_authenticated:
		flash('You are currently logged in', 'danger')
		return redirect(url_for('index'))
	if request.method == 'POST':
		username = request.form['username']
		password = request.form['password']
		if (not username or username.isspace()) or (not password or password.isspace()):
			flash('Please fill all required fields', 'warning')
			return render_template('signup.html')
		if User.query.filter_by(username=username).first() is None:
			try:
				new_user = User(username, password)
				db.session.add(new_user)
				db.session.commit()
				login_user(new_user)
				flash('User created', 'success')
			except:
				db.session.rollback()
				flash('Something gone wrong. Please try again', 'warning')
			return redirect(url_for('index'))
		flash('Username is already in use', 'danger')
	return render_template('signup.html')


@app.route('/logout/', methods=['GET', 'POST'])
@login_required
def logout():
	try:
		user = current_user
		user.release_robots()
		user.logout()
		db.session.commit()
		logout_user()
		flash('You were successfully logged out', 'success')
	except:
		db.session.rollback()
		flash('Something gone wrong. Please try again', 'warning')
	return redirect(url_for('index'))


@app.route('/robots/', methods=['GET', 'POST'])
@login_required
def list_robots():
	robots = Robot.query.order_by(Robot.id.asc()).all()
	return render_template('robots.html', robots=robots)


@app.route('/users/', methods=['GET'])
@login_required
def users():
	if not current_user.is_admin():
		flash('Operation not permited.', 'danger')
		return redirect(url_for('index'))
	users_list = User.query.order_by(User.date_registred.asc()).all()
	return render_template('users.html', users=users_list)


@app.route('/users/change_state/', methods=['POST'])
@login_required
def user_change_state():
	result = dict(error=False, message="")
	if not current_user.is_admin():
		result["error"] = True
		result["message"] = "Operation not permitted."
	else:
		user_id = request.values.get('id')
		if user_id is not None:
			user = User.query.filter_by(id=user_id).first()
			if user is None:
				result["error"] = True
				result["message"] = "User with id " + user_id + " doesn't exists"
			elif not user.is_admin():
				try:
					user.change_state()
					db.session.commit()
				except:
					db.session.rollback()
					result["error"] = True
					result["message"] = "Something gone wrong. Please try again"
			else:
				result["error"] = True
				result["message"] = "Operation not permitted on admins."
	return jsonify(result)


@app.route('/robots/id/<int:robot_id>', methods=['GET', 'POST'])
@login_required
def control_robot(robot_id):
	robot = Robot.query.get(robot_id)
	if robot is not None:
		if robot.is_active():
			if robot.is_in_use():
				if not robot.can_release_control(current_user.id):
					flash('Robot is already in use, we\'re sorry for the inconvenience.', 'warning')
					return redirect(url_for('list_robots'))
			if robot.user_id is None:
				if not current_user.is_admin():
					try:
						current_user.release_robots()
						robot.control(current_user.id)
						db.session.commit()
					except:
						db.session.rollback()
						flash('Something gone wrong. Please try again', 'warning')
			return render_template('robots-control.html', robot=robot)
		flash('Robot is offline now, we\'re sorry for the inconvenience.', 'warning')
	else:
		flash('Robot you\'re looking for is no longer avaible. Please check the id.', 'warning')
	return redirect(url_for('list_robots'))


@app.route('/robots/name/<string:robot_name>', methods=['GET', 'POST'])
@login_required
def control_robot_by_name(robot_name):
	robot = Robot.query.filter_by(name=robot_name.lower()).first()
	if robot is not None:
		return control_robot(robot.id)
	flash('Robot you\'re looking for is no longer avaible. Please check the name.', 'warning')
	return redirect(url_for('list_robots'))


@app.route('/robots/release/id/<int:robot_id>', methods=['GET', 'POST'])
@login_required
def release_control(robot_id):
	if robot_id is not None:
		robot = Robot.query.get(robot_id)
		if robot.can_release_control(current_user.id):
			robot.release_control()
			db.session.commit()
			flash('Robot released successfully', 'success')
		else:
			flash('You are not using this robot', 'danger')
		return redirect(url_for('list_robots'))
	else:
		flash('Wrong ID.', 'danger')
	return 'Releasing control'


@app.route('/robots/release/name/<string:robot_name>', methods=['GET', 'POST'])
@login_required
def release_control_by_name(robot_name):
	robot = Robot.query.filter_by(name=robot_name.lower()).first()
	if robot is not None:
		return release_control(robot.id)
	flash('Robot you\'re looking for is no longer avaible. Please check the name.', 'warning')
	return redirect(url_for('list_robots'))


@app.route('/tokenregister/', methods=['POST'])
def token_register():
	token = request.values.get("token")
	name = request.values.get("name")
	response = dict(error=False, message="")
	if token is not None:
		robot = Robot.query.filter_by(name=name).first()

		if robot is None:
			robot2 = Robot.query.filter_by(token=token).first()
			if robot2 is not None:
				robot2.name = name
				db.session.add(robot2)
			else:
				robot = Robot(name, "Spain", True)
				robot.token = token
			db.session.add(robot)
			db.session.commit()
			response["message"] = "Registred"
		elif robot.token == token:
			response["message"] = "Authorized"
		else:
			response["error"] = True
			response["message"] = "Name already in use"

	return jsonify(response)


@login_required
@app.route('/gcm/downstream/', methods=['POST'])
def send_gcm_msg_robot():
	result = dict(error=False, message="")
	if current_user.is_active():
		robot_name = request.values.get("robot")
		message = request.values.get("message")
		orders = {"UP", "DOWN", "LEFT", "RIGHT", "STOP"}
		if message is not None and message in orders and robot_name is not None:
			robot = Robot.query.filter_by(name=robot_name.lower()).first()
			if robot is not None:
				send_gcm_downstream(robot.token, dict(message=message))
		else:
			result["error"] = True
			result["message"] = "Invalid token or message."
	else:
		result["error"] = True
		result["message"] = "Your user is currently banned, please contact admins."
	return jsonify(result)


@app.route('/gcm/downstream/token/<string:token>/message/<string:msg>', methods=['GET', 'POST'])
def send_gcm_msg(token, msg):
	message = {"message": msg}
	send_gcm_downstream(token, message)
	return "Successfully sent. Message sent: " + msg


def send_gcm_downstream(reg_id, data):
	gcm = GCM(get_api_key())
	gcm.plaintext_request(registration_id=reg_id, data=data)


if __name__ == '__main__':
	if app.debug:
		app.run(host='0.0.0.0')
	else:
		app.run()
