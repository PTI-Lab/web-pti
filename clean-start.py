import os

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def module_exists(module_name):
    try:
        __import__(module_name)
    except ImportError:
        return False
    else:
        return True


if not module_exists('dotenv'):
    print (bcolors.HEADER + "Installing module dependencies...")
    os.system("pip install -U python-dotenv")

from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

print(bcolors.OKBLUE + "     1) REMOVING ALL EXISTING CONTAINERS\n" + bcolors.ENDC)
os.system("docker rm --force `docker ps -qa`")
os.system('docker-compose build')
if os.environ.get("ENV") == "dev":
    print(bcolors.HEADER + bcolors.BOLD + "\nSETTING UP DEVELOPER MODE:\n" + bcolors.ENDC)
    os.system('docker-compose up -d')
    print(bcolors.OKBLUE + "     2) CONTAINERS BUILD" + bcolors.ENDC)

else:
    print(bcolors.HEADER + bcolors.BOLD + "\nSETTING UP PRODUCTION MODE:\n" + bcolors.ENDC)
    os.system("docker-compose -f docker-compose-prod.yml up -d")
    print(bcolors.OKBLUE + "     2) CONTAINERS BUILD" + bcolors.ENDC)

print(bcolors.OKGREEN + "         >>> CONTAINERS BUILD FINISHED\n" + bcolors.ENDC)
print(bcolors.OKBLUE + "     3) RECREATING DATABASE" + bcolors.ENDC)
print("          Waiting 5s...")
os.system("sleep 5")
os.system("docker-compose run web /usr/local/bin/python create_db.py")
print(bcolors.OKGREEN + ">> OK! SETUP FINISHED CORRECTLY" + bcolors.ENDC)
